package com.atlassian.intellij.plugin.almond.indexer;

import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.util.indexing.*;
import com.intellij.util.io.EnumeratorStringDescriptor;
import com.intellij.util.io.KeyDescriptor;
import org.jetbrains.annotations.NotNull;

public class JSRequireIndex extends ScalarIndexExtension<String> {

    private static final String JS_EXTENSION = "js";

    public static final ID<String, Void> REQUIRE_INDEXER_ID = ID.create("com.atlassian.intellij.plugin.almond.require.id");
    private static final DataIndexer<String, Void, FileContent> REQUIRE_DATA_INDEXER = new JSRequireDataIndexer();
    private static final FileBasedIndex.InputFilter ALMOND_FILTER = new FileBasedIndex.InputFilter()
    {
        @Override
        public boolean acceptInput(@NotNull VirtualFile virtualFile)
        {
            // Will be null if it's scanning folder
            return virtualFile.getExtension() != null && virtualFile.getExtension().equals(JS_EXTENSION);
        }
    };

    @NotNull
    @Override
    public ID<String, Void> getName() {
        return REQUIRE_INDEXER_ID;
    }

    @NotNull
    @Override
    public DataIndexer<String, Void, FileContent> getIndexer() {
        return REQUIRE_DATA_INDEXER;
    }

    @NotNull
    @Override
    public KeyDescriptor<String> getKeyDescriptor() {
        return new EnumeratorStringDescriptor();
    }

    @NotNull
    @Override
    public FileBasedIndex.InputFilter getInputFilter() {
        return ALMOND_FILTER;
    }

    @Override
    public boolean dependsOnFileContent() {
        return true;
    }

    @Override
    public int getVersion() {
        return 0;
    }
}
