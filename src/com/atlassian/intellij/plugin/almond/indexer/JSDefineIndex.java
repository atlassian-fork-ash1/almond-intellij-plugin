package com.atlassian.intellij.plugin.almond.indexer;


import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.util.indexing.*;
import com.intellij.util.io.EnumeratorStringDescriptor;
import com.intellij.util.io.KeyDescriptor;
import org.jetbrains.annotations.NotNull;

public class JSDefineIndex extends ScalarIndexExtension<String>
{

    private static final String JS_EXTENSION = "js";

    public static final ID<String, Void> ALMOND_INDEXER_ID = ID.create("com.atlassian.intellij.plugin.almond.indexer.id");
    private static final DataIndexer<String, Void, FileContent> ALMOND_DEFINE_INDEXER = new JSDefineDataIndexer();
    private static final FileBasedIndex.InputFilter ALMOND_FILTER = new FileBasedIndex.InputFilter()
    {
        @Override
        public boolean acceptInput(@NotNull VirtualFile virtualFile)
        {
            // Will be null if it's scanning folder
            return virtualFile.getExtension() != null && virtualFile.getExtension().equals(JS_EXTENSION);
        }
    };

    @NotNull
    @Override
    public ID<String, Void> getName()
    {
        return ALMOND_INDEXER_ID;
    }

    @NotNull
    @Override
    public DataIndexer<String, Void, FileContent> getIndexer()
    {
        return ALMOND_DEFINE_INDEXER;
    }

    @NotNull
    @Override
    public KeyDescriptor<String> getKeyDescriptor()
    {
        return new EnumeratorStringDescriptor();
    }

    @NotNull
    @Override
    public FileBasedIndex.InputFilter getInputFilter()
    {
        return ALMOND_FILTER;
    }

    @Override
    public boolean dependsOnFileContent()
    {
        return true;
    }

    @Override
    public int getVersion()
    {
        return 0;
    }

}
