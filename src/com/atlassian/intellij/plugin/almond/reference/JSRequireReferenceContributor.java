package com.atlassian.intellij.plugin.almond.reference;

import com.intellij.lang.javascript.psi.JSLiteralExpression;
import com.intellij.patterns.PlatformPatterns;
import com.intellij.psi.*;
import com.intellij.util.ProcessingContext;
import org.jetbrains.annotations.NotNull;

import static com.atlassian.intellij.plugin.almond.utils.ElementPatterns.isDefineCallExpression;
import static com.atlassian.intellij.plugin.almond.utils.ElementPatterns.isDefineDeclaration;
import static com.atlassian.intellij.plugin.almond.utils.ElementPatterns.isRequireCallExpression;

public class JSRequireReferenceContributor extends PsiReferenceContributor
{
    @Override
    public void registerReferenceProviders(@NotNull PsiReferenceRegistrar psiReferenceRegistrar)
    {
        psiReferenceRegistrar.registerReferenceProvider(PlatformPatterns.psiElement(JSLiteralExpression.class),
                new PsiReferenceProvider()
                {
                    @NotNull
                    @Override
                    public PsiReference[] getReferencesByElement(@NotNull PsiElement element, @NotNull ProcessingContext context)
                    {
                        if (isDefineCallExpression(element) || isRequireCallExpression(element))
                        {
                            JSRequireReference jsRequireReference = new JSRequireReference(element);
                            if (jsRequireReference.canResolve())
                            {
                                return new PsiReference[]{jsRequireReference};
                            }
                            else
                            {
                                return new PsiReference[0];
                            }
                        }

                        if (isDefineDeclaration(element))
                        {
                            return new PsiReference[]{new JSDefineReference(element)};
                        }

                        return new PsiReference[0];
                    }
                });
    }
}
